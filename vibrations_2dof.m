% Author : Aldair E. Gongora 
% Date   : March 7, 2017 
% Class  : ME 515 Vibrations of Complex Mech Sys 
% Topic  : Quiz 4 
clear all 
close all
clc 
%% Parameter  Description 
% [m]: mass (kg) 
% [c]: damping (N/(m/s)
% [k]: spring constant (N/m)
% [L]: length (m)
%% Matrices 
mass_matrix =[1 0 0; 0 1 0; 0 0 1];
damping_matrix =[3 -1 -1; -1 3 -1; -1 -1 2];
stiffness_matrix =[2 -1 0; -1 2 -1; 0 -1 1];
force_matrix = [1;1;1];
%% Preliminaries 
size = numel(force_matrix);
num_points = 200;
max_omega = 2; %Hz
%omega = (0.0001:0.0001:max_omega); 
omega = linspace(0,max_omega,num_points); 
time = (0.1:0.01:1000);
m = mass_matrix; 
c = damping_matrix; 
k = stiffness_matrix; 
f = force_matrix;
w = omega;
%% Solving for Steady State Response 

%% Solving for Amplitudes 
for i = 1:num_points 
    w(i) = 2*pi*w(i); % rad/s
    x(:,i) = (-w(i)^2*m-1i*w(i)*c+k)\f; 
    amp_x(:,i) = sqrt(real(x(:,i)).^2 + imag(x(:,i)).^2);
    phase_auto(:,i) = atan2(imag(x(:,i)),real(x(:,i)))*180/pi; % atan2 output in degrees  
      
    %%    
%     phase_track(:,i) = phase_auto(:,i); 
%     phase_check = phase_auto(:,i)
%     
%     for j = 1:size       
%         if imag(x(j,i)) > 0.00000 && real(x(j,i)) < 0.000000
%             phase_check(j) = -(180 + phase_check(j));
%         elseif imag(x(j,i)) < 0.00000 && real(x(j,i)) < 0.000000
%             phase_check(j) = 180 - phase_check(j);
%             a = 5
%         else
%             phase_check(j) = phase_check(j); 
%         end
%     end 
%     phase_auto(:,i) = phase_check; 
     phase(:,i) = phase_auto(:,i);
    
    
%    for j = 1:size
%         if phase_check(j) <180 && phase_check(j) >90
%             phase_check(j) = 180- phase_check(j);
%         elseif phase_check(j) < -90.000 && phase_check(j) > -180.000
%             phase_check(j) = -(180 + phase_check(j)); 
%         else
%             phase_check(j) = phase_check(j); 
%         end
            
%    end
%    phase(:,i) = phase_check;  
%    
   
   
%     phase(:,i) = -atan(imag(x(:,i))./real(x(:,i)))*180/pi; % Data plotted (updated later) 
%     phase_raw(:,i) = phase(:,i); % -atan output 
%     
%      if phase(1,i) < 0 
%          phase(1,i) = phase(1,i) + 180; 
%      else 
%          phase(1,i) = phase(1,i); 
%      end

%       if phase(1,i)<0 
%           if imag(x(1,i))<0
%               phase(1,i) = phase(1,i) - 180;
%           else
%               phase(1,i) = phase(1,i) + 180;
%           end
%       else
%           phase(1,i) = phase(1,i);            
%       end 
%       if phase(2,i)<0 
%           if imag(x(2,i))<0
%               phase(2,i) = phase(2,i) - 180;
%           else
%               phase(2,i) = phase(2,i) + 180;
%           end
%       else
%           phase(2,i) = phase(2,i);            
%       end 
      
end
%% Plotting 
for i = 1: size
    subplot(size,2,2*i-1)
    plot(omega,amp_x(i,:));
    grid
    xlabel('Frequency (Hz)')
    ylabel(['Amplitude of Mass ',num2str(i),' (m)']) 
    subplot(size,2,2*i)
    plot(omega,phase(i,:));
    grid
    xlabel('Frequency (Hz)')
    ylabel (['Phase of Mass ',num2str(i),' '])
end 