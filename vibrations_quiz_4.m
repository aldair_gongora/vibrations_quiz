% Author : Aldair E. Gongora 
% Date   : March 7, 2017 
% Class  : ME 515 Vibrations of Complex Mech Sys 
% Topic  : Quiz 4 
clear all 
close all
clc 
%% Parameter  Description 
% [m]: mass (kg) 
% [c]: damping (N/(m/s)
% [k]: spring constant (N/m)
% [L]: length (m)
m_1 = 1; 
m_2 = 1; 
m_3 = 1; 
k = 1; 
c = 1; 
L = 1; 
g = 10;
%% Matrices 
mass_matrix =[m_1 0 0; 0 (m_2+m_3) m_3*L; 0 1 L];
damping_matrix =[c -c 0; -c c 0; 0 0 0];
stiffness_matrix =[k -k 0; -k k 0; 0 0 g];
force_matrix = [1;0;0];
m = mass_matrix; 
c = damping_matrix; 
k = stiffness_matrix; 
f = force_matrix;
counter = 1;
max_w = 1; %Hz
min_w = 0.00001; %Hz
step_size = .0001; 
for w = min_w:step_size:max_w
    w = 2*pi*w; % Hz to rad/s 
    x = (-w^2*m+1i*w*c+k)\f; 
    x_track(:,counter) = (-w^2*m-1i*w*c+k)\f; 
    amp_x(:,counter) = sqrt(real(x).^2 + imag(x).^2);
    z(:,counter) = (1i*w*x)\f; 
    z_real(:,counter) = real(z(1,counter)); 
    z_imag(:,counter) = imag(z(1,counter)); 
    counter = counter + 1; 
end
w_10 = 10;
w_10 = 2*pi*w_10; % Hz to rad/s
x_10 = (-w^2*m+1i*w*c+k)\f;
amp_x_10 = sqrt(real(x_10).^2 + imag(x_10).^2);
phase_10 = angle(x_10); 
start_time = 0;
time_step = 0.001; 
final_time = 2; 
counter_2 = 1;
for time = start_time:time_step:final_time; 
    x_n_10(1,counter_2) = amp_x_10(1)*cos(w_10*time-phase_10(1));
    x_n_10(2,counter_2) = amp_x_10(2)*cos(w_10*time-phase_10(2));
    x_n_10(3,counter_2) = amp_x_10(3)*cos(w_10*time-phase_10(3));
    counter_2 = counter_2 +1;
end 
%% PLOTTING
w = min_w:step_size:max_w; 
time = start_time:time_step:final_time; 
% Mass 1 
figure(1)
plot(w,amp_x(1,:))
%title('Mass 1 Amplitude of Vibration')
xlabel('Frequency (Hz)')
ylabel('Amplitude (m)')
print -depsc mass1_plot.eps
figure(2)
semilogy(w,amp_x(1,:));
%title('Mass 1 Amplitude of Vibration') 
xlabel('Frequency (Hz)')
ylabel('Amplitude (m)')
print -depsc mass1_semilogy_plot.eps
% Mass 2
figure(3)
plot(w,amp_x(2,:))
%title('Mass 2 Amplitude of Vibration')
xlabel('Frequency (Hz)')
ylabel('Amplitude (m)')
print -depsc mass2_plot.eps
figure(4)
semilogy(w,amp_x(2,:));
%title('Mass 2 Amplitude of Vibration') 
xlabel('Frequency (Hz)')
ylabel('Amplitude (m)')
print -depsc mass2_semilogy_plot.eps
% Mass 3 
figure(5)
plot(w,amp_x(3,:))
%title('Mass 3 Amplitude of Vibration')
xlabel('Frequency (Hz)')
ylabel('Amplitude (rad)')
print -depsc mass3_plot.eps
figure(6)
semilogy(w,amp_x(3,:));
%title('Mass 3 Amplitude of Vibration') 
xlabel('Frequency (Hz)')
ylabel('Amplitude (rad)')
print -depsc mass3_semilogy_plot.eps
% Complex-valued impedance Z 
figure(7)
plot(w,z_real);
%title('Complex-Valued Impedance Z')
xlabel('Frequency (Hz)')
ylabel('Real part of Impedance')
print -depsc impedance_real_plot.eps
figure(8)
plot(w,z_imag); 
%title('Complex-Valued Impedance Z')
xlabel('Frequency (Hz)') 
ylabel('Imaginary part of Impedance') 
print -depsc impedance_imag_plot.eps
% Steady State Response Mass 1
figure(9) 
plot(time,x_n_10(1,:))
%title('Steady State Response')
xlabel('Time (s)')
ylabel('Displacement (m)')
print -depsc steadystate_mass1_plot.eps
% Steady State Response Mass 2
figure(10) 
plot(time,x_n_10(2,:))
%title('Steady State Response')
xlabel('Time (s)')
ylabel('Displacement (m)')
print -depsc steadystate_mass2_plot.eps
% Steady State Response Mass 3
figure(11) 
plot(time,x_n_10(1,:))
%title('Steady State Response')
xlabel('Time (s)')
ylabel('Displacement (rad)')
print -depsc steadystate_mass3_plot.eps